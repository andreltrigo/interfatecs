#!/usr/bin/env python3

n = int(input())
p = [int(input()) for i in range(n)]

paridade = False
for posicao in p:
    paridade ^= False if posicao == 1 else True

print("impar" if paridade else "par")

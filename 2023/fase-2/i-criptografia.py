#!/usr/bin/env python3

def primos(limite: int) -> tuple:
    if limite < 2: return tuple()

    n = (limite - 1) // 2
    crivo = n * [True]

    for i in range(3, limite + 1, 2):
        for j in range((i * i - 2) // 2, n, i):
            crivo[j] = False

    return (2,) + tuple(2 * (i + 1) + 1 for i in range(n) if crivo[i])

def fatorar(n: int) -> tuple:
    raiz = int(n ** 0.5)
    fatores = []

    for divisor in primos(raiz + 1):
        while n % divisor == 0:
            fatores.append(divisor)
            n //= divisor

        if n == 1: break
    else:
        fatores.append(n)

    return tuple(fatores)

def cifrar(senha: str) -> str:
    soma = 0
    for i in range(len(senha)):
        soma += ord(senha[i]) * (i + 1)

    cifra = f"{soma}" + "".join(f"{n}" for n in fatorar(soma))
    return cifra

cadastro = []
while (entrada := input()) != "ACABOU":
    usuario, senha = entrada.split()
    cadastro.append((usuario, cifrar(senha)))

for registro in sorted(cadastro):
    usuario, cifra = registro
    print(f"usuario...: {usuario}")
    print(f"valor hash: {cifra}")
    print(30 * '-')

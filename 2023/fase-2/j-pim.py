#!/usr/bin/env python3

n = int(input())
print(*("pim" if n % 4 == 0 else n for n in range(1, n + 1)))

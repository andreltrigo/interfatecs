#!/usr/bin/env python3

v = int(input())

print(f"{v * 1.07 if v > 107 else v + 7:.0f}")

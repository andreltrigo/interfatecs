#!/usr/bin/env python3

def vezes_lidos(livros: str, n: int) -> list:
    livros = list(livros)
    vezes = dict(zip(livros, n * [0]))

    for i in range(n - 1):
        for j in range(i + 1, n):
            if livros[i] > livros[j]:
                livros[i], livros[j] = livros[j], livros[i]
                vezes[livros[j]] += 1

    return list(vezes.values())

n = int(input())
livros = input()

vezes = vezes_lidos(livros, n)
if all(n <= 5 for n in vezes):
    print(sum(vezes))
else:
    print("A Database Systems student read a book.")

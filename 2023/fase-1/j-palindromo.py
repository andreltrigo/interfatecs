#!/usr/bin/env python3

from re import sub
from unicodedata import normalize

def palindromo(string: str) -> bool:
    e, d = 0, len(string) - 1
    while e < d:
        if string[e] != string[d]:
            return False

        e += 1
        d -= 1

    return True

eof = False
while not eof:
    try:
        frase = input().lower()
    except EOFError:
        eof = True
        continue

    frase = "".join(c for c in normalize("NFD", frase) if c.isascii())
    frase = sub(r" |^[^a-z0-9]+|[^a-z0-9]+$", "", frase)

    if palindromo(frase):
        mensagem = "Parabens, voce encontrou o Palindromo Perdido!"
    else:
        mensagem = "A busca continua, o Palindromo Perdido ainda nao foi encontrado."
    print(mensagem)

#!/usr/bin/env python3

from string import ascii_letters

letras = {"minusculas": ascii_letters[:26],
          "maiusculas": ascii_letters[26:]}

n, p = input().split()

for i in range(1, int(n) + 1):
    print((26 - i) * '.' + letras[p][:i])

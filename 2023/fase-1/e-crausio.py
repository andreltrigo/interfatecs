#!/usr/bin/env python3

l, c, b = [int(n) for n in input().split()]
x, y = [int(n) for n in input().split()]
r = input()

batidas = 0
for comando in r:
    if b == 0:
        break

    match comando:
        case 'B' if x < l:
            x += 1
        case 'C' if x > 1:
            x -= 1
        case 'D' if y < c:
            y += 1
        case 'E' if y > 1:
            y -= 1
        case _:
            batidas += 1
    b -= 1

print(x, y, batidas)

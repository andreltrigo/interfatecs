#!/usr/bin/env python3

while (n := int(input())) > 0:
    quadrados = [n * n for n in range(1, int(n ** 0.5) + 1)]
    print(*quadrados)
